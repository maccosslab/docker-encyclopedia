################## BASE IMAGE ######################
FROM openjdk:11-jre

# Install openMP for percolator v3
RUN apt-get update && \
    apt-get install -y --no-install-recommends libgomp1

RUN mkdir -p /usr/local/encyclopedia/bin /usr/local/encyclopedia/java /data && chmod -R 777 /data

WORKDIR /usr/local/encyclopedia/java

RUN curl -OL https://bitbucket.org/searleb/encyclopedia/downloads/encyclopedia-0.8.0-executable.jar

WORKDIR /usr/local/encyclopedia/bin

COPY encyclopedia /usr/local/encyclopedia/bin/

RUN chmod a+x encyclopedia

WORKDIR /data

ENV CLASSPATH "$CLASSPATH:/usr/local/encyclopedia/java/encyclopedia-0.8.0-executable.jar"
ENV PATH "$PATH:/usr/local/encyclopedia/bin"

################## METADATA ######################
LABEL base_image="openjdk:11-jre"
LABEL version="2"
LABEL software="EncyclopeDIA"
LABEL software.version="0.8.0"
LABEL about.summary="Library searching for Data-Independent Acquisition"
LABEL about.home="https://bitbucket.org/searleb/encyclopedia/wiki/Home"
LABEL about.documentation="https://bitbucket.org/searleb/encyclopedia/wiki/Home"
LABEL about.license_file="https://www.apache.org/licenses/LICENSE-2.0"
LABEL about.license="SPDX:Apache-2.0"
LABEL about.tags="Proteomics"
LABEL extra.identifiers.biotools="encyclopedia"

################## MAINTAINER ######################
MAINTAINER Austin Keller <atkeller@uw.edu>

# Specify java options using the JAVA_OPTS environment variable, e.g. JAVA_OPTS="-Xmx8G"
